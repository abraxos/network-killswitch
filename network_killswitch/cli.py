from subprocess import run, CalledProcessError
from traceback import format_exc
from typing import Iterable, Optional
from time import sleep

from typeguard import typechecked
import click
from psutil import NoSuchProcess

from .netstat import pids_for_name
from .netstat import net_connections_for_pid, net_connections_for_name
from .netstat import laddrs_to_interfaces_for_pid, laddrs_to_interfaces_for_name
from .log import log


@typechecked
def execute_kill(kill_cmd: str) -> None:
    try:
        cmd = ['/bin/bash', '-c', kill_cmd]
        log.debug(cmd)
        run(cmd)
    except CalledProcessError:
        log.warning(f'Unable to execute: {kill_cmd}')
        log.warning(format_exc())


@typechecked
def process_pid(pid: int, interfaces: Iterable[str], kill_cmd: str) -> None:
    try:
        log.info(f'Checking PID: {pid} network connections...')
        for conn in net_connections_for_pid(pid):
            log.debug(f'\tConn: {conn.laddr.ip if conn.laddr else "-"}:'
                      f'{conn.laddr.port if conn.laddr else "-"} -> '
                      f'{conn.raddr.ip if conn.raddr else "-"}:'
                      f'{conn.raddr.port if conn.raddr else "-"}')
        log.info('\tInterface mappings:')
        for ip, ifaces in laddrs_to_interfaces_for_pid(pid,
                                                       interfaces).items():
            log.info(f'\t\tIP: {ip} - {", ".join(i for i in ifaces)}')
        outliers = [ip for ip, ifaces in
                    laddrs_to_interfaces_for_pid(pid, interfaces).items()
                    if ip and not ifaces]
        if outliers:
            log.critical(f'IPs were detected that do not belong to an '
                         f'allowed interface: {", ".join(outliers)}')
            log.critical(f"Killing process PID: {pid}")
            cmd = kill_cmd.format(pid=pid)
            log.info(f"Executing: {cmd}")
            execute_kill(cmd)
    except NoSuchProcess:
        log.warning(f"It appears as though {pid} has stopped while we "
                    f"were trying to work on it.")


@typechecked
def process_name(name: str, interfaces: Iterable[str], kill_cmd: str) -> None:
    try:
        log.info(f'Checking process name: {name} network connections...')
        for conn in net_connections_for_name(name):
            log.debug(f'\tConn: {conn.laddr.ip if conn.laddr else "-"}:'
                      f'{conn.laddr.port if conn.laddr else "-"} -> '
                      f'{conn.raddr.ip if conn.raddr else "-"}:'
                      f'{conn.raddr.port if conn.raddr else "-"}')
        log.info('\tInterface mappings:')
        for ip, ifaces in laddrs_to_interfaces_for_name(name,
                                                        interfaces).items():
            log.info(f'\t\tIP: {ip} - {", ".join(i for i in ifaces)}')
        outliers = [ip for ip, ifaces in
                    laddrs_to_interfaces_for_name(name, interfaces).items()
                    if ip and not ifaces]
        if outliers:
            log.critical(f'IPs were detected that do not belong to an '
                         f'allowed interface: {", ".join(outliers)}')
            pids = list(pids_for_name(name))
            if pids:
                log.critical(f"Killing process ({name}) PIDs: "
                             f"{', '.join(str(pid) for pid in pids)}")
                log.debug(kill_cmd)
                if kill_cmd.format(pid='$$123$$') != kill_cmd:
                    log.debug(pids)
                    for pid in pids:
                        cmd = kill_cmd.format(pid=pid)
                        log.info(f"Executing: {cmd}")
                        execute_kill(cmd)
                else:
                    log.info(f"Executing: {kill_cmd}")
                    execute_kill(cmd)
            else:
                log.critical(f'No PIDs found for {name}...')
    except NoSuchProcess:
        log.warning(f"It appears as though {name} has stopped while we"
                    f" were trying to work on it.")


def run_killswitch(pids: Optional[Iterable[int]],
                   names: Optional[Iterable[str]],
                   interfaces: Iterable[str],
                   kill_cmd: str) -> None:
    if pids is not None:
        for pid in pids:
            process_pid(pid, interfaces, kill_cmd)
    if names is not None:
        for name in names:
            process_name(name, interfaces, kill_cmd)


@click.command()
@click.option('-i', '--interface', 'interfaces', type=str, multiple=True,
              required=True,
              help='The interface on which a given process is allowed to '
                   'communicate')
@click.option('-p', '--pid', 'pids', type=int, multiple=True,
              help='The PID of a process that should be checked')
@click.option('-n', '--name', 'names', type=str, multiple=True,
              help='The name of a process that should be checked')
@click.option('--kill-command', type=str, multiple=False, default='kill {pid}',
              help='The command that should be used to kill the process. You '
                   'can insert the PID of the process to be killed with '
                   '"{pid}" like "kil -9 {pid}" or you can give a general '
                   'command like "service my-service stop". The default '
                   'kill-command is "kill {pid}" and if you provide a process '
                   'name(s) or pid(s) that apply to multiple processes, each '
                   'of those found to be outside the given network interface '
                   'will be killed.')
@click.option('--poll', type=int, default=None, multiple=False,
              help='Indicates that the script should run continuously and poll'
                   ' for network information on a given process with a given'
                   ' interval in seconds')
@typechecked
def main(interfaces: Iterable[str], pids: Optional[Iterable[int]],
         names: Optional[Iterable[str]], kill_command: str,
         poll: Optional[int]) -> None:
    """A script to kill processes if it can observe them communicating
       on a network interface other than the one that is given. An interface
       and a PID or process name must be provided."""
    if not pids and not names:
        log.critical('No PIDs or process names provided')
        exit(1)
    if poll is not None:
        while 42:
            run_killswitch(pids, names, interfaces, kill_command)
            sleep(poll)
    else:
        run_killswitch(pids, names, interfaces, kill_command)
