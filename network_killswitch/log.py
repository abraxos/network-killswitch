import logging
import logging.config
import os
from pathlib import Path

import yaml

SEARCH_PATHS = [
    Path.home() / '.config' / 'delivery-service' / 'log.yaml',
    Path(os.path.realpath(__file__)).parent / 'log.yaml',
]  # in order of preference

for config_path in SEARCH_PATHS:
    if config_path.exists():
        with config_path.open() as f:
            logging.config.dictConfig(yaml.safe_load(f.read()))
            break

log = logging.getLogger(__name__)  # pylint: disable=C0103
log.debug('Logging initialized')
