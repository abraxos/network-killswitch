from os.path import basename
from typing import Iterable, Dict
from collections import namedtuple
from itertools import chain

from psutil import pid_exists, process_iter, Process
from psutil import net_if_addrs
from typeguard import typechecked


pconn = namedtuple('pconn', ['fd', 'family', 'type', 'laddr', 'raddr',
                             'status'])


@typechecked
def is_proc_named(proc: Process, candidate: str) -> bool:
    return bool(candidate == proc.name() or
                (proc.exe() and basename(proc.info['exe']) == candidate) or
                (proc.cmdline() and proc.cmdline()[0] == candidate))


@typechecked
def procs_by_name(name: str) -> Iterable[Process]:
    return (p for p in process_iter(attrs=["name",
                                           "exe",
                                           "cmdline"]) if is_proc_named(p,
                                                                        name))


@typechecked
def pids_for_name(name: str) -> Iterable[int]:
    return (p.pid for p in procs_by_name(name))


@typechecked
def pids_for_names(names: Iterable[str]) -> Iterable[int]:
    return chain.from_iterable(pids_for_name(name) for name in names)


@typechecked
def net_connections_for_pid(pid: int) -> Iterable[pconn]:
    return (c for c in Process(pid).connections() if c.raddr) \
        if pid_exists(pid) else []


@typechecked
def net_connections_for_name(name: str) -> Iterable[pconn]:
    return chain.from_iterable(net_connections_for_pid(p)
                               for p in pids_for_name(name))


@typechecked
def addrs_for_iface(iface: str) -> Iterable[str]:
    return (i.address for i in net_if_addrs()[iface])\
           if iface in net_if_addrs() else []


@typechecked
def laddrs_for_pid(pid: int) -> Iterable[str]:
    return (c.laddr.ip for c in net_connections_for_pid(pid))


@typechecked
def laddrs_for_name(name: str) -> Iterable[str]:
    return (c.laddr.ip for c in net_connections_for_name(name))


@typechecked
def laddrs_for_pid_outside_of_interface(pid: int, iface: str) -> Iterable[str]:
    return (i for i in set(laddrs_for_pid(pid)) - addrs_for_iface(iface))


@typechecked
def laddrs_for_name_outside_of_interface(name: str,
                                         iface: str) -> Iterable[str]:
    return (i for i in set(laddrs_for_name(name)) - addrs_for_iface(iface))


@typechecked
def laddrs_to_interfaces_for_pid(
        pid: int, interfaces: Iterable[str]) -> Dict[str, Iterable[str]]:
    iface_to_addrs = {i: set(addrs_for_iface(i)) for i in interfaces}
    return {ip: set(i for i in iface_to_addrs if ip in iface_to_addrs[i])
            for ip in laddrs_for_pid(pid)}


@typechecked
def laddrs_to_interfaces_for_name(
        name: str, interfaces: Iterable[str]) -> Dict[str, Iterable[str]]:
    iface_to_addrs = {i: set(addrs_for_iface(i)) for i in interfaces}
    return {ip: set(i for i in iface_to_addrs if ip in iface_to_addrs[i])
            for ip in laddrs_for_name(name)}
