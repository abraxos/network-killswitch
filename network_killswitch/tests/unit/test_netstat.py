from funcy import first
from psutil import net_if_addrs
from typeguard import typechecked

from network_killswitch.netstat import addrs_for_iface


@typechecked
def test_addrs_for_iface() -> None:
    assert '127.0.0.1' in addrs_for_iface(first(net_if_addrs().keys()))


@typechecked
def test_addrs_for_nonexistent_iface() -> None:
    assert not addrs_for_iface('non-existent-interface')
