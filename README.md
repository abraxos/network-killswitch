# Network Interface Kill-Switch

A command-line utility for ensuring that a given application is only talking on an explicitly permitted network interface. If any deviation is discovered the process is killed. It can be used either as a one-off call, or for constantly polling the set of open network connections.

_Caution! This tool is meant as a sanity check or a "last line of defense" against potentially bad configurations. It should not be dependent on to somehow prevent connections, but rather kill processes **after** they are discovered as doing something wrong. It can **easily** miss connections while polling if those connections occur inbetween the polls._

## Setup

### Installation

Clone this repo and then execute the following:

```
sudo pip3 install /path/to/repo
```

### Development Setup

**Setting up the virtualenv**

Install a virtualenv and virtualenvwrapper according to the instructions here:

+ https://virtualenv.pypa.io/en/stable/
+ https://virtualenvwrapper.readthedocs.io/en/latest/

Then execute:

```
mkvirtualenv --python=$(which python3) network-killswitch
```

**Installing the Package**

```
(network-killswitch) pip install -e /path/to/repo
```

The `-e` installs the package in place for development.


## Usage

Execute: `nik --help` to get a printout of all the functionality and command-line parameters.

### Logging Configuration

The Network Killswitch comes with a default logging configuration which can be seen in `network-killswitch/network_killswitch/log.yaml` which logs to STDOUT. The configuration format is pretty standard Python logging configuration which is described here: https://docs.python.org/3/howto/logging.html#logging-basic-tutorial.

If you would like to change it to also log to a file, for example, you can place something like this in `~/.config/network-killswitch/log.yaml`

```
version: 1
formatters:
  simple:
    format: '[%(asctime)s] %(levelname)s: %(message)s'
handlers:
  console:
    class: logging.StreamHandler
    level: DEBUG
    formatter: simple
    stream: ext://sys.stdout
  file:
    class: logging.handlers.RotatingFileHandler
    level: DEBUG
    formatter: simple
    filename: /home/eugene/.config/network-killswitch/main.log
    maxBytes: 10485760 # 10MB
    backupCount: 20
    encoding: utf8
loggers:
  sampleLogger:
    level: DEBUG
    handlers: [console]
    propagate: no
root:
  level: DEBUG
  handlers: [console, file]
```