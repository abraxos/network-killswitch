"""Setup file for network-killswitch"""
from setuptools import setup, find_packages


TEST_REQUIREMENTS = ['pytest', 'pytest-cov', 'pylint', 'mypy', 'flake8']

with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='network-killswitch',
    version='0.1.0',
    description='A network traffic monitoring service that is intended to act'
                ' as a failsafe if a program ever communicates outside of a '
                'given network interface for any reason.',
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene@kovalev.systems',
    url='https://gitlab.com/abraxos/network-killswitch',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={'console_scripts': ['nik=network_killswitch.cli:main']},
    install_requires=REQUIREMENTS + TEST_REQUIREMENTS,
)
